import json
import logging
import re
import sys
from os import PathLike


from flywheel_bids.flywheel_bids_app_toolkit.commands import validate_kwargs

log = logging.getLogger(__name__)


def check_eddy_config(eddy_config_path: PathLike, subject_label: str, session_label: str):
    """If eddy_config.json is provided, substitute subject and session labels."""
    search_dict = {"subject_label": subject_label, "session_label": session_label}
    with open(eddy_config_path, "r+") as f:
        contents = json.load(f)

        for k, v in search_dict.items():
            regex = re.compile(rf"\${{{k}}}", re.S)
            # contents['args']=regex.sub(lambda m: m.group().replace(v),contents['args'])
            contents["args"] = regex.sub(v, contents["args"])
            # make sure there are no doubled sub, ses strs
            regex_sub = re.compile("sub-sub", re.S)
            regex_ses = re.compile("ses-ses", re.S)
            contents["args"] = regex_sub.sub("sub", contents["args"])
            contents["args"] = regex_ses.sub("ses", contents["args"])
        f.seek(0)
        json.dump(contents, f, indent=4)
        f.truncate()


def check_recon_args(gear_context, app_context):
    if gear_context.config.get("recon-only", None) and gear_context.get_input_path("recon-input"):
        app_context["recon-input"] = gear_context.get_input_path("recon-input")
    elif gear_context.config.get("recon-only", None):
        log.error("--recon-only selected, but recon-input unspecified. Exiting.")
        sys.exit(1)
    elif gear_context.get_input_path("recon-input"):
        log.error("recon-input provided, but recon-only not selected. Please check and create a new gear run.")
        sys.exit(1)


def validate_setup(gear_context, app_context):
    """Customizable validation pipeline for gear-dependent configuration options.

    The goal is to cause the gear to fail quickly and with useful debugging help
    prior to downloading BIDS data or running (doomed) BIDS algorithms.

    Validating the bids_app_options kwargs should be included for all BIDS App gears.
    Other items to consider for validation include:
    1) Do any input files require other input files?
    2) Do other modification scripts/methods need to be run on inputs?
    3) Are any config settings mutually exclusive and need to be double-checked?
    """
    if app_context.bids_app_options:
        validate_kwargs(app_context)
    if gear_context.config.get("recon-only") or gear_context.get_input_path("recon-input"):
        check_recon_args(gear_context, app_context)
    if gear_context.get_input_path("eddy_config.json"):
        check_eddy_config(
            gear_context.get_input_path("eddy_config.json"), app_context.subject_label, app_context.session_label
        )
