#################################################################
# Develop the venv for Flywheel (flypy) in a modern python image.
# Older versions of Ubuntu, used by some BIDS Apps, are not
# compatible with modern pip. The mismatch headache can be
# avoided by creating a venv and copying the venv to the old
# Ubuntu base.
##################################################################
FROM python:3.10-slim as fw_base
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean
RUN apt-get install --no-install-recommends -y \
   git \
   build-essential \
   zip \
   nodejs \
   tree && \
   apt-get clean && \
   rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN python -m venv /opt/flypy
ENV PATH="/opt/flypy/bin:$PATH"

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install -U pip
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

COPY ./ $FLYWHEEL/
RUN pip install --no-cache-dir .

# Isolate the versions of the dependencies within the BIDS App
# from the (potentially updated) Flywheel dependencies by copying
# the venv with the pip installed Flyhweel deps.

# The template BIDS app repo is used for the testing example here.
### REPLACE WITH THE BIDS APP OFFICIAL IMAGE
FROM pennbbl/qsiprep:0.20.0 as bids_runner
ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Install build dependencies
RUN apt-get update && \
    apt-get install -y  \
    curl \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libncurses5-dev \
    libncursesw5-dev \
    libreadline-dev \
    libsqlite3-dev \
    libgdbm-dev \
    libdb5.3-dev \
    libbz2-dev \
    libexpat1-dev \
    liblzma-dev \
    libffi-dev \
    uuid-dev \
    && \
    apt-get clean

RUN cd /tmp && \
    curl -O https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz && \
    tar xzf Python-3.10.0.tgz

# Build and install fw_base's version of python
RUN cd /tmp/Python-3.10.0 && \
    ./configure && \
    make && \
    make altinstall

COPY --from=fw_base /opt/flypy /opt/flypy
# Update the softlink to point to fw_base's version of python in bids_runner
RUN ln -sf /usr/local/bin/python3.10 /opt/flypy/bin/python
# "Re-copy the gear's guts
# NOTE: The deps should be captured in /opt/flypy
COPY ./ $FLYWHEEL/

## Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["/opt/flypy/bin/python", "/flywheel/v0/run.py"]