# QSIprep Gear

This gear runs [QSIprep](https://qsiprep.readthedocs.io/) on
[BIDS-curated data](https://bids.neuroimaging.io/).

For a description of what QSIprep does, read the
[official documentation](https://qsiprep.readthedocs.io/).

This gear runs the official
[`pennbbl/qsiprep:0.20.0` Docker image](https://hub.docker.com/r/pennbbl/qsiprep)

# Update the BIDS algorithm version
1. Fork the repo.
2. [Update the DockerHub image](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-qsiprep/-/blob/main/Dockerfile?ref_type=heads#L42) that the gear will use.
3. Update the [gear-builder line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-qsiprep/-/blob/main/manifest.json?ref_type=heads#L78) in the manifest.
4. Update the [version line](https://gitlab.com/flywheel-io/scientific-solutions/gears/bids-apps/bids-qsiprep/-/blob/main/manifest.json?ref_type=heads#L203) in the manifest.
5. Run `poetry update` from the local commandline (where your cwd is the top-level of the gear). This command will update any dependencies for the Flywheel portion of the gear (not the BIDS algorithm itself!).
6. Run `fw-beta gear build` to update anything in the manifest.
7. Ideally, run `fw-beta gear upload` and complete a test run on your Flywheel instance.
8. Run `git checkout -b {my-update}`, `git commit -a -m "{My message about updating}" -n`, and `git push`.
9.  Submit a merge request (MR) to the original gear repo for Flywheel staff to review for official inclusion in the exchange.

## Overview

[Usage](#usage)

[FAQ](#faq)

### Summary

`qsiprep` configures pipelines for processing diffusion-weighted MRI (dMRI) data. The
main features of this software are:

1. A BIDS-app approach to preprocessing nearly all kinds of modern diffusion MRI data.
2. Automatically generated preprocessing pipelines that correctly group, distortion
   correct,
   motion correct, denoise, coregister and resample your scans, producing visual reports
   and QC metrics.
3. A system for running state-of-the-art reconstruction pipelines that include
   algorithms
   from Dipy, MRTrix, DSI Studio and others.
4. A novel motion correction algorithm that works on DSI and random q-space sampling
   schemes

### Cite

**license:**

* QSIPrep: [BSD-3-Clause](https://github.com/PennLINC/qsiprep/blob/0.15.4/LICENSE)
* Dependencies:
    * FSL: <https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/Licence>
    * ANTs: <https://github.com/ANTsX/ANTs/blob/v2.2.0/COPYING.txt>
    * AFNI: <https://afni.nimh.nih.gov/pub/dist/doc/program_help/README.copyright.html>
    * DSI-Studio: <https://dsi-studio.labsolver.org/download.html>
    * MRtrix3: <https://github.com/MRtrix3/mrtrix3/blob/master/LICENCE.txt>

**url:** <https://gitlab.com/flywheel-io/flywheel-apps/bids-qsiprep>

**cite:** <https://qsiprep.readthedocs.io/en/latest/citing.html>

### Classification

*Category:* analysis

*Gear Level:*

- [ ] Project
- [x] Subject
- [x] Session
- [ ] Acquisition
- [ ] Analysis

----

[[_TOC_]]

----

### Inputs

* bidsignore
    * **Name**: bidsignore
    * **Type**: object
    * **Optional**: true
    * **Classification**: file
    * **Description**: A .bidsignore file to provide to the bids-validator that this
      gear
      runs before running the main command.

* eddy-config
    * **Name**: eddy-config
    * **Type**: object
    * **Optional**: true
    * **Classification**: file
    * **Description**: Path to a json file with settings for the call to eddy. If no
      json
      is specified, a default one will be used. The current default json can be found
      here:
      <https://github.com/PennBBL/qsiprep/blob/master/qsiprep/data/eddy_params.json>

* freesurfer_license_file
    * **Name**: freesurfer_license_file
    * **Type**: object
    * **Optional**: true
    * **Classification**: file
    * **Description**: FreeSurfer license file, provided during registration with
      FreeSurfer. This file will be copied to the `$FSHOME` directory and used during
      execution of the Gear.

* api-key
    * **Name**: api-key
    * **Type**: object
    * **Optional**: true
    * **Classification**: api-key
    * **Description**: Flywheel API key.

* recon-spec
    * **Name**: recon-spec
    * **Type**: object
    * **Optional**: true
    * **Classification**: file
    * **Description**: json file specifying a reconstruction pipeline to be run after
      preprocessing.

- archived_runs
    - OPTIONAL
    - __Type__: file
    - If there are other files that will allow the algorithm to run or pick up from a
      certain point, then the archive can be supplied here. This zip will be
      automatically unzipped within the gear.

### Config

- bids_app_command
    - OPTIONAL
    - __Type__: free-text
    - The gear will run the defaults for the algorithm without anything in this box. If
      you wish to provide the command as you would on a CLI, input the exact command
      here. Flywheel will automatically update the BIDS_dir, output_dir, and
      analysis_level. (
      e.g., `bids_app bids_dir output participant --valid-arg1 --valid-arg2`)
    - For more help to build the command, please see https://qsiprep.readthedocs.io/en/latest/usage.html
    - Note: If you use a kwarg here, don't worry about putting a value in the box for
      the same kwarg below. Any kwarg that you see called out in the config UI and that
      is given a value will supersede the kwarg:value given as part of the
      bids_app_command.

- debug
    - __Type__: Boolean
    - __Default__: false
    - Verbosity of log messages; default results in INFO level, True will log DEBUG
      level

- gear-dry-run
    - __Type__: Boolean
    - __Default__: false (set to true for template)
    - Do everything related to Flywheel except actually execute the `qsiprep` command.
    - Note: This is NOT the same as running the BIDS app command with `--dry-run`.
      gear-dry-run will not actually download the BIDS data, attempt to run the BIDS app
      command, or do any metadata/result updating.

- gear-post-processing-only
    - __Type__: Boolean
    - __Default__: false
    - If an archive file is available, one can pick up with a previously analyzed
      dataset. This option is useful if the metadata changed (or needs to change). It is
      also useful for development.

### Outputs

#### Files
bids_tree.html

- Description of the available BIDS files and structure

bids-qsiprep_qsiprep_{destination_container}.zip

- Output files from the algorithm

qsiprep_log.txt

- Log from the QSIPrep algorithm

job.log

- Details on the gear execution on Flywheel


#### Metadata

Any notes on metadata created by this gear

### Pre-requisites

This BIDS-App runs FreeSurfer, so you need to provide a valid FreeSurfer license.

Supported ways to provide the license are documented [here](
https://docs.flywheel.io/hc/en-us/articles/360013235453)

#### Prerequisite Gear Runs

BIDS-curate must run successfully in order for any BIDS app gear to be able to download
the proper data.

#### Prerequisite Files

Reference [Inputs](#Inputs) for various files that are acceptable as inputs, depending on individual use cases and workflows.

#### Prerequisite Metadata

A description of any metadata that is needed for the gear to run.
If possible, list as many specific metadata objects that are required:

1. BIDS
    - {container}.info.BIDS
    - Enables `export_bids` to download the proper data

## Usage

BIDS-QSIPrep will run with very little intervention from the user. After selecting a
session and clicking "Run Gear", selections may be made from the GUI to change
default behavior or the user may start the gear without changing anything.

Previous processing that BIDS-QSIPrep would normally look for in the BIDS directory may be
supplied in the Inputs tab under "archived_runs". This file will be unzipped into
the BIDS working directory.

In the Configuration tab, one may provide the commandline input you would run in a
terminal for BIDS-QSIPrep in the free-text bids_app_command field. The BIDS App Toolkit parses
any arguments beyond the typical `app_name BIDS_dir output_dir
analysis_level` when creating the BIDSAppContext object (see flywheel_bids.flywheel_bids_app_toolkit.context.parse_bids_app_commands), so additional
commandline arguments will be passed to the algorithm. Additionally, the gear checks
the arguments against the BIDS-QSIPrep usage file early in the gear run to fail quickly, if
an errant argument is provided.

### Description

This gear is run at either the `Subject` or the `Session` level. It downloads the data
for that subject/session into the `/flwyhweel/v0/work/bids` folder and then runs the
`QSIprep` pipeline on it.

After the pipeline is run, the output folder is zipped and saved into the analysis
container.


### Workflow

A picture and description of the workflow

```mermaid
  graph LR;
    A[T1w]:::input --> FW;
    B[dwi]:::input --> FW;
    C[fmap]:::input --> FW;
    FW[FW] --> FMI;
    FMI((file-metadata-importer)):::gear --> FC;
    FC((file-classifier)):::gear --> D2N;
    D2N((dcm2niix)):::gear --> CB;
    CB((curate-bids)):::gear --> BQP;
    BQP((BIDS-QSIPrep)):::gear --> ANA;
    ANA[Analysis]:::container;
    
    classDef container fill:#57d,color:#fff
    classDef input fill:#7a9,color:#fff
    classDef gear fill:#659,color:#fff
```

Description of workflow

1. Upload data to container
2. Prepare data by running the following gears:
    1. file metadata importer
    2. file classifier
    3. dcm2niix
    4. MRIQC (optional)
    5. curate bids
3. Select either a subject or a session.
4. Run the BIDS-QSIPrep gear
5. Gear places output in Analysis

### Use Cases

Please, refer to the [official documentation](
https://qsiprep.readthedocs.io/en/latest/preprocessing.html)

### Logging

Debug is the highest number of messages and will report some locations/options and all
errors. If unchecked in the configuration tab, only INFO and higher priority level log
messages will be reported.

## FAQ

[FAQ.md](FAQ.md)

## Contributing

[For more information about how to get started contributing to that gear,
checkout [CONTRIBUTING.md](CONTRIBUTING.md).]
<!-- markdownlint-disable-file -->
