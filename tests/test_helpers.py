import pytest
from unittest.mock import patch
from fw_gear_bids_qsiprep.utils.helpers import validate_setup


@pytest.mark.parametrize(
    "app_options, input_dict, mvk_call_count, mra_call_count, " "mec_call_count",
    [
        ({"app_opt1": "present"}, {}, 1, 0, 0),
        ({}, {"recon-input": "a/file"}, 0, 1, 0),
        ({}, {"eddy_config.json": "a/json"}, 0, 0, 1),
        ({"app_opt200": "wow, that is a lot"}, {"recon-input": "yep", "eddy_config.json": "new_json"}, 1, 1, 1),
    ],
)
@patch("fw_gear_bids_qsiprep.utils.helpers.check_eddy_config")
@patch("fw_gear_bids_qsiprep.utils.helpers.check_recon_args")
@patch("fw_gear_bids_qsiprep.utils.helpers.validate_kwargs")
def test_validate_setup(
    mock_validate_kwargs,
    mock_recon_args,
    mock_eddy_config,
    app_options,
    input_dict,
    mvk_call_count,
    mra_call_count,
    mec_call_count,
    extended_gear_context,
    mock_app_context,
):
    mock_app_context.bids_app_options = app_options
    extended_gear_context.get_input_path.side_effect = lambda k: input_dict.get(k, None)
    validate_setup(extended_gear_context, mock_app_context)
    assert mock_validate_kwargs.call_count == mvk_call_count
    assert mock_recon_args.call_count == mra_call_count
    assert mock_eddy_config.call_count == mec_call_count
